module.exports = {
  "express": {
    "endpoint": "/test",
    "hostname": "localhost",
    "port": "8080"
  },
  "mysqldb": {
    "database": "test",
    "multipleStatements": true,
    "host": "localhost",
    "user": "root",
    "password": "1234"
  },
  "developerMode": true,
  "secretKey": "ñla8923e67f"
};
