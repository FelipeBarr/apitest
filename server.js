const fs = require("fs");
const app = require("./app");
const config = require("./config");

if (!config.httpsOptions) {
  var https = require("http").createServer(app);
} else {
  httpsOptions = {
    key: fs.readFileSync(config.httpsOptions.key),
    cert: fs.readFileSync(config.httpsOptions.cert),
  };
  var https = require("https").createServer(httpsOptions, app);
}

https.listen(config.express.port, config.express.hostname, () => {
  console.log("Server running in port:", config.express.port);
});
