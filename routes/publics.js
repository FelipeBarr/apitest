const express = require('express');
const db = require('../database/queryFunctions');
const Ajv = require('ajv');
const ajv = new Ajv();
const config = require('../config');
var jwt = require('jsonwebtoken'); // used to create, sign, and verify tokens
const responseThrower = require('../libs/responseThrower.js').responseThrower;
var generator = require('generate-password');
const router = express.Router();

router.post('/products', (req, res) => {
  console.log("pene");
  let data = req.body;
  let schema = {
    $async: true,
    additionalProperties: false,
    type: 'object',
    properties: {
      name: { type: 'string', minLength: 1 },
      price: { type: 'number', minimum: 1},
      type: { type: 'string'}
    },
  required: ['name', 'price', 'type'],
  };
  ajv.validate(schema, data).then(
    async () => {
      data.is_active = true;
      db.product.productCreate(data).then((result) => {
          responseThrower(res, req.logId, 200, { result });
      },
      (error) => {
        console.error('error :', error);
        responseThrower(res, req.logId, 500, {
          error: 'somethingWrongInServer',
        });
      })
    },
    (error) => {
      console.log(data);
      console.log(error);
      responseThrower(res, req.logId, 400, error);
    }
  );
});

router.get('/products/:start_date', (req, res) => {
  var data = req.params;
  const start_dateTime = new Date(data.start_date);
  start_dateTime.setHours(start_dateTime.getHours() + 6);
  data.start_date = start_dateTime.toISOString();

  let schema = {
    $async: true,
    additionalProperties: false,
    type: 'object',
    properties: {
      start_date: { type: 'string', format: 'date-time' }
    },
    required: ['start_date'],
  };
  ajv.validate(schema, data).then(
    () => {
      db.product.getProducts(data).then(
        (result) => {
          responseThrower(res, req.logId, 200, { result: result });
        },
        (error) => {
          console.error(error);
          responseThrower(res, req.logId, 500, {
            error: 'somethingWrongInServer',
          });
        }
      );
    },
    (error) => {
      console.error(error);
      responseThrower(res, req.logId, 400, error);
    }
  );
});



module.exports = router;
