var knex = require("./knex");

const thisTable = 'product';

async function productCreate (data) {
  try {
    const insertData = { ...data };
    const product_id = await insertProduct(insertData);
    const product_info = await selectProduct(product_id);

    if (product_info.length){
      return(product_info);
    } 
    else {
      return { items: []};
    }
  } catch (error) {
    console.error(error.message);
  }
};

async function getProducts (data) {
  try {
    const products = await filterProduct(data.start_date);
    if (products.length){
      return(products);
    } 
    else {
      return { items: []};
    }
  } catch (error) {
    console.error(error.message);
  }
};

const insertProduct = (data) => {
  return new Promise((resolve, reject) => {
    knex(thisTable)
    .insert(data)
    .then(
      (product) => {
        resolve(product);
      },
      (error) => {
        reject(error);
      }
    );
  });
}

const selectProduct = (data) => {
  return new Promise((resolve, reject) => {
    knex(thisTable)
    .select(
      'id',
      'name',
      'price',
      'type',
      'is_active',
      'is_deleted',
      'created_on'
    )
    .where({ id: data})
    .where({ is_deleted: false})
    .then(
      (product) => {
        resolve(product);
      },
      (error) => {
        reject(error);
      }
    );
  });
}

const filterProduct = (data) => {
  return new Promise((resolve, reject) => {
    knex(thisTable)
    .select(
      'id',
      'name',
      'price',
      'type',
      'is_active',
      'is_deleted',
      'created_on'
    )
    .where('created_on', '>=', data)
    .then(
      (product) => {
        resolve(product);
      },
      (error) => {
        reject(error);
      }
    );
  });
}

module.exports.productCreate = productCreate;
module.exports.getProducts= getProducts;



