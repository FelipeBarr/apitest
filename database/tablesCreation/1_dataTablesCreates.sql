DROP DATABASE IF EXISTS test;
CREATE DATABASE test;
USE test;

DROP TABLE IF EXISTS product;
CREATE TABLE product (
  -- specific  fields
  id              INT               NOT NULL    AUTO_INCREMENT,
  name            TEXT              NOT NULL,
  price           FLOAT             NOT NULL,
  type            TEXT              NOT NULL,
  

  -- common fields
  is_active   BOOLEAN     NOT NULL    DEFAULT true,
  is_deleted  BOOLEAN     NOT NULL    DEFAULT false,
  created_on  TIMESTAMP   NOT NULL    DEFAULT CURRENT_TIMESTAMP,

  -- keys
  PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;






