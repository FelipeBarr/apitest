-- Script para crear tablas
source ./database/tablesCreation/1_dataTablesCreates.sql
-- Spript usado para las tablas que relacionan varios con varios. En Rethink seria el equivalente de guardar arrays.
-- source ./database/tablesCreation/2_relationshipTablesCreates.sql 
-- Script para crear las tablas de historiales
-- source ./database/tablesCreation/3_historyTablesCreates.sql
-- Script que agrega datos
source ./database/tablesCreation/4_dataInsertion.sql
-- source ./database/tablesCreation/messageTables.sql
