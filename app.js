const bodyParser = require("body-parser"); //parser of the body
const cors = require("cors");
const middleware = require("./libs/middleware");
const morgan = require("morgan"); //logging dev
const express = require("express"); //routing and middleware functions
const config = require("./config");
const app = express();
const appEndpoint = config.express.endpoint;

app.use(cors());
app.use(morgan("combined"));
app.use(bodyParser.json({ limit: "15mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "15mb" }));
app.use(middleware.storageInit);

//#region public routes section
app.use(appEndpoint, require('./routes/publics'));
//#endregion

//#region middlewares section
var routes = [];
//app.use(middleware.tokenValidator); // token validator
//app.use(middleware.permissionsValidator);
app.use(middleware.routesValidator(routes));
//#endregion

//#region private routes section
//#endregion

require("./libs/listRoutes")(app._router.stack).map((i) => routes.push(i));

module.exports = app;
