var Ajv = require("ajv");
var ajv = new Ajv({ $data: true, useDefaults: true });
require("ajv-keywords")(ajv, ["formatMinimum", "formatMaximum"]);
var db = require("../database/queryFunctions");
var fs = require('fs');

const isEqual = (obj1, obj2) => {
  const obj1Keys = Object.keys(obj1);
  const obj2Keys = Object.keys(obj2);

  if (obj1Keys.length !== obj2Keys.length) {
    return false;
  }

  for (let objKey of obj1Keys) {
    if (obj1[objKey] !== obj2[objKey]) {
      return false;
    }
  }

  return true;
};

ajv.ValidationError = Ajv.ValidationError;

ajv.addKeyword("appropiatePermissions", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (schema.properties != [] && data.permissions != undefined) {
        var validprops = schema.properties;
        Object.keys(data.permissions).forEach((item) => {
          if (!Object.keys(validprops).includes(item)) {
            resolve(false);
          }
        });
        Object.keys(data.permissions).forEach((item) => {
          for (var i = 0; i < data.permissions[item].length; i++) {
            if (validprops[item].includes(data.permissions[item].charAt(i))) {
            } else {
              console.log("Permisos no correctos");
              resolve(false);
            }
          }
          resolve(true);
        });
      } else {
        console.log("Schema no provisto de tabla o propiedades.");
        resolve(true);
      }
    }),
});

var countries = [
  "Afganistán",
  "Albania",
  "Alemania",
  "Andorra",
  "Angola",
  "Antigua y Barbuda",
  "Arabia Saudita",
  "Argelia",
  "Argentina",
  "Armenia",
  "Australia",
  "Austria",
  "Azerbaiyán",
  "Bahamas",
  "Bangladés",
  "Barbados",
  "Baréin",
  "Bélgica",
  "Belice",
  "Benín",
  "Bielorrusia",
  "Birmania",
  "Bolivia",
  "Bosnia y Herzegovina",
  "Botsuana",
  "Brasil",
  "Brunéi",
  "Bulgaria",
  "Burkina Faso",
  "Burundi",
  "Bután",
  "Cabo Verde",
  "Camboya",
  "Camerún",
  "Canadá",
  "Catar",
  "Chad",
  "Chile",
  "China",
  "Chipre",
  "Ciudad del Vaticano",
  "Colombia",
  "Comoras",
  "Corea del Norte",
  "Corea del Sur",
  "Costa de Marfil",
  "Costa Rica",
  "Croacia",
  "Cuba",
  "Dinamarca",
  "Dominica",
  "Ecuador",
  "Egipto",
  "El Salvador",
  "Emiratos Árabes Unidos",
  "Eritrea",
  "Eslovaquia",
  "Eslovenia",
  "España",
  "Estados Unidos",
  "Estonia",
  "Etiopía",
  "Filipinas",
  "Finlandia",
  "Fiyi",
  "Francia",
  "Gabón",
  "Gambia",
  "Georgia",
  "Ghana",
  "Granada",
  "Grecia",
  "Guatemala",
  "Guyana",
  "Guinea",
  "Guinea ecuatorial",
  "Guinea-Bisáu",
  "Haití",
  "Honduras",
  "Hungría",
  "India",
  "Indonesia",
  "Irak",
  "Irán",
  "Irlanda",
  "Islandia",
  "Islas Marshall",
  "Islas Salomón",
  "Israel",
  "Italia",
  "Jamaica",
  "Japón",
  "Jordania",
  "Kazajistán",
  "Kenia",
  "Kirguistán",
  "Kiribati",
  "Kuwait",
  "Laos",
  "Lesoto",
  "Letonia",
  "Líbano",
  "Liberia",
  "Libia",
  "Liechtenstein",
  "Lituania",
  "Luxemburgo",
  "Macedonia del Norte",
  "Madagascar",
  "Malasia",
  "Malaui",
  "Maldivas",
  "Malí",
  "Malta",
  "Marruecos",
  "Mauricio",
  "Mauritania",
  "México",
  "Micronesia",
  "Moldavia",
  "Mónaco",
  "Mongolia",
  "Montenegro",
  "Mozambique",
  "Namibia",
  "Nauru",
  "Nepal",
  "Nicaragua",
  "Níger",
  "Nigeria",
  "Noruega",
  "Nueva Zelanda",
  "Omán",
  "Países Bajos",
  "Pakistán",
  "Palaos",
  "Panamá",
  "Papúa Nueva Guinea",
  "Paraguay",
  "Perú",
  "Polonia",
  "Portugal",
  "Reino Unido",
  "República Centroafricana",
  "República Checa",
  "República del Congo",
  "República Democrática del Congo",
  "República Dominicana",
  "República Sudafricana",
  "Ruanda",
  "Rumanía",
  "Rusia",
  "Samoa",
  "San Cristóbal y Nieves",
  "San Marino",
  "San Vicente y las Granadinas",
  "Santa Lucía",
  "Santo Tomé y Príncipe",
  "Senegal",
  "Serbia",
  "Seychelles",
  "Sierra Leona",
  "Singapur",
  "Siria",
  "Somalia",
  "Sri Lanka",
  "Suazilandia",
  "Sudán",
  "Sudán del Sur",
  "Suecia",
  "Suiza",
  "Surinam",
  "Tailandia",
  "Tanzania",
  "Tayikistán",
  "Timor Oriental",
  "Togo",
  "Tonga",
  "Trinidad y Tobago",
  "Túnez",
  "Turkmenistán",
  "Turquía",
  "Tuvalu",
  "Ucrania",
  "Uganda",
  "Uruguay",
  "Uzbekistán",
  "Vanuatu",
  "Venezuela",
  "Vietnam",
  "Yemen",
  "Yibuti",
  "Zambia",
  "Zimbabue",
];

var entity = [
  "Aguascalientes",
  "Baja California",
  "Baja California Sur",
  "Campeche",
  "Chiapas",
  "Chihuahua",
  "Ciudad de México",
  "Coahuila",
  "Colima",
  "Durango",
  "México",
  "Guanajuato",
  "Guerrero",
  "Hidalgo",
  "Jalisco",
  "Michoacán",
  "Morelos",
  "Nayarit",
  "Nuevo León",
  "Oaxaca",
  "Puebla",
  "Querétaro",
  "Quintana Roo",
  "San Luis Potosí",
  "Sinaloa",
  "Sonora",
  "Tabasco",
  "Tamaulipas",
  "Tlaxcala",
  "Veracruz",
  "Yucatán",
  "Zacatecas",
];

ajv.addKeyword("validateCountry", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (countries.includes(data)) {
        resolve(true);
      } else {
        reject(
          new Ajv.ValidationError([
            {
              keyword: "countryDoesNotExist",
              dataPath: ".country",
              params: data,
            },
          ])
        );
      }
    }),
});

ajv.addKeyword("validateQuestionId", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if('questions' in rootData){
        var found = false
        for (i in rootData.questions){
          if(rootData.questions[i].question_in_survey_id==data){
            found=true;
          }
        }
        if(found){
          resolve(true)
        }else{
          reject(
            new Ajv.ValidationError([
              {
                keyword: "question_id_missing",
                dataPath: currentPath
            },
            ])
          );
        }
      }else{
        resolve(true)
      }
    }),
});

ajv.addKeyword("checkLogo", {
  async: true,
  validate: (
    schema,
    data
  ) =>
    new Promise((resolve, reject) => {
      data='./surveyPhotosPreview/'+data;
      fs.access(data, fs.F_OK, (err) => {
        if (err) {
          console.error(err)
          reject(
            new Ajv.ValidationError([
              {
                keyword: "itemDoesNotExist",
                dataPath: ".logo"
            },
            ])
          );
          return
        }
        resolve(true)
        //true
      })
    }),
});

ajv.addKeyword("validateQuestion", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      switch (data.question_type) {
        case "section":
          var linear_scale_schema = {
            "$async": true,
            "additionalProperties": false,
            "type": "object",
            "properties": {
              "is_new":{"type": "boolean"},
              "id":{"type": "integer", "validateExistence": { "table": "survey_question" }},
              "is_deleted":{"type": "boolean"},
              "is_obligatory": { "type": "boolean" },
              "question_type": { "type": "string" },
              "question": { "type": "string" },
              "question_in_survey_id":{"type":"string"},
              "description": { "type": "string" },
              "aux":{"type": "string"}
            },
            "required": ["question_type", "question", "description"]
          };
          ajv.validate(linear_scale_schema, data).then((valid) => {
            resolve(true);
          }, (error) => {
            reject(error);
          });
          break;
        case "lineal-scale":
          var linear_scale_schema = {
            "$async": true,
            "additionalProperties": false,
            "type": "object",
            "properties": {
              "is_new":{"type": "boolean"},
              "id":{"type": "integer", "validateExistence": { "table": "survey_question" }},
              "is_deleted":{"type": "boolean"},
              "question_type": { "type": "string" },
              "is_obligatory": { "type": "boolean" },
              "question_in_survey_id":{"type":"string"},
              "slicer_type": { "type": "string", "enum": ["none", "stars", "emoticons"] },
              "max_options": { "type": "integer", "minimum": 2, "maximum": 20 },
              "question": { "type": "string" },
              "description": { "type": "string" },
            },
            "required": ["question_type", "is_obligatory", "slicer_type", "max_options", "question"]
          };
          ajv.validate(linear_scale_schema, data).then((valid) => {
            if (data.slicer_type == "stars" || "emoticons") data.max_options = 5
            resolve(true);
          }, (error) => {
            reject(error);
          });
          break;
        case "yes-no":
        case "short_answer":
        case "paragraph":
          var simple_answer_schema = {
            "$async": true,
            "additionalProperties": false,
            "type": "object",
            "properties": {
              "is_new":{"type": "boolean"},
              "id":{"type": "integer", "validateExistence": { "table": "survey_question" }},
              "is_deleted":{"type": "boolean"},
              "question_type": { "type": "string" },
              "is_obligatory": { "type": "boolean" },
              "question_in_survey_id":{"type":"string"},
              "question": { "type": "string" },
              "description": { "type": "string" },
            },
            "required": ["question_type", "is_obligatory", "question"]
          };
          ajv.validate(simple_answer_schema, data).then((valid) => {
            resolve(true);
          }, (error) => {
            reject(error);
          });
          break;
        case "unique-option":
        case "multiple-option":
          var option_list_schema = {
            "$async": true,
            "additionalProperties": false,
            "type": "object",
            "properties": {
              "is_new":{"type": "boolean"},
              "id":{"type": "integer", "validateExistence": { "table": "survey_question" }},
              "is_deleted":{"type": "boolean"},
              "question_type": { "type": "string" },
              "is_obligatory": { "type": "boolean" },
              "question_in_survey_id":{"type":"string"},
              "option_list": { "type": "array", "maxItems":10, "minItems":1, "items":{ "type": "string"}  },
              "use_others": { "type": "boolean" },
              "others_name": { "type": "string", "default": "otros"},
              "question": { "type": "string" },
              "description": { "type": "string" },
            },
            "required": ["question_type", "is_obligatory", "option_list", "use_others", "others_name", "question"]
          };
          ajv.validate(option_list_schema, data).then((valid) => {
            resolve(true);
          }, (error) => {
            reject(error);
          });
          break;
        case "matrix":
          var matrix_schema = {
            "$async": true,
            "additionalProperties": false,
            "type": "object",
            "properties": {
              "is_new":{"type": "boolean"},
              "id":{"type": "integer", "validateExistence": { "table": "survey_question" }},
              "is_deleted":{"type": "boolean"},
              "question_type": { "type": "string" },
              "is_obligatory": { "type": "boolean" },
              "question_in_survey_id":{"type":"string"},
              "row_list": { "type": "array", "maxItems":10, "minItems":1, "items":{ "type": "string"}  },
              "column_list": { "type": "array", "maxItems":10, "minItems":1, "items":{ "type": "string"}  },
              "question": { "type": "string" },
              "description": { "type": "string" },
            },
            "required": ["question_type", "is_obligatory", "row_list", "column_list", "question"]
          };
          ajv.validate(matrix_schema, data).then((valid) => {
            if (data.slicer_type == "stars" || "emoticons") data.max_options = 5
            resolve(true);
          }, (error) => {
            reject(error);
          });
          break;
      }
    }),
});

ajv.addKeyword("validateEntity", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (entity.includes(data)) {
        resolve(true);
      } else {
        reject(
          new Ajv.ValidationError([
            {
              keyword: "entityDoesNotExist",
              dataPath: ".entity",
              params: data,
            },
          ])
        );
      }
    }),
});

ajv.addKeyword("robot_role_relation", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (schema) {
        db.robotRolePermissions.findRelation(data, rootData.id).then(
          (result) => {
            if (result.length > 0) {
              if (result[0].permissions) {
                console.log(result[0].permissions, "???");
                var dbPerm = JSON.parse(result[0].permissions);
                if (isEqual(dbPerm, data.permissions)) {
                  data.status = "keep";
                  resolve(true);
                } else {
                  data.RRP_id = result[0].RRP;
                  data.status = "update";
                  resolve(true);
                }
              } else {
                data.status = "new";
                resolve(true);
              }
            } else {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "itemDoesNotExist",
                  },
                ])
              );
            }
          },
          (error) => {
            resolve(error, 500, {
              error: "SomethingWrongInServer",
              params: error,
            });
          }
        );
      }
    }),
});

ajv.addKeyword("robot_product_relation", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (schema) {
        db.allowedProducts.findRelation(data, rootData.id).then(
          (result) => {
            if (result.length > 0) {
              if (result[0].is_active !== null) {
                if (result[0].is_active == data.is_active) {
                  data.status = "keep";
                  resolve(true);
                } else {
                  data.AP_id = result[0].AP;
                  data.status = "update";
                  resolve(true);
                }
              } else {
                data.status = "new";
                resolve(true);
              }
            } else {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "itemDoesNotExist",
                  },
                ])
              );
            }
          },
          (error) => {
            resolve(error, 500, {
              error: "SomethingWrongInServer",
              params: error,
            });
          }
        );
      }
    }),
});

ajv.addKeyword("uniqueRobotId", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      function hasDuplicates(array) {
        return new Set(array).size !== array.length;
      }
      var robot_ids = [];
      for (i in data) {
        robot_ids.push(data[i].robot_id);
      }
      if (schema) {
        if (!hasDuplicates(robot_ids)) {
          resolve(true);
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "repeted_robot_id",
              },
            ])
          );
        }
      }
    }),
});

ajv.addKeyword("xyOptionalHome", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data.type == "home") {
        if (data.hasOwnProperty("x")) {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "additionalPropertiesInHome",
                dataPath: "./stops[].x && ./stops[].y && ./stops[].phi",
              },
            ])
          );
        } else {
          resolve(true);
        }
      } else {
        if (data.hasOwnProperty("x")) {
          resolve(true);
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "requiredInNotHome",
                dataPath: "./stops[].x && ./stops[].x && ./stops[].phi",
              },
            ])
          );
        }
      }
    }),
});

ajv.addKeyword("monthLimit", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (schema) {
        var end_date = new Date(rootData.end_date);
        var start_date = new Date(data);
        var limitDate = end_date.setMonth(end_date.getMonth() - schema);
        if (start_date >= limitDate) {
          resolve(true);
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "longPeriod",
                dataPath: ".start_Date",
                params: { limit: "3 months" },
              },
            ])
          );
        }
      }
    }),
});

ajv.addKeyword("validateUniqueness", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (schema) {
        console.log("--", schema, "--", data, "--");
        db.common.findObject(schema, data).then(
          (result) => {
            console.log("result :", result);
            if (result) {
              resolve(true);
            } else {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                    dataPath: "." + schema.properties,
                    params: data[schema.properties],
                  },
                ])
              );
            }
          },
          (error) => {
            resolve(error, 500, {
              error: "SomethingWrongInServer",
              params: error,
            });
          }
        );
      }
    }),
});

ajv.addKeyword("validateUniquenessEmail", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common
          .getMatchingUsersEmail(data, originalId)
          .then(function (matchingItems) {
            if (matchingItems.totalFound > 0) {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                  },
                ])
              );
            } else {
              resolve(true);
            }
          });
      } else {
        console.log("Email not provided");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateUniquenessPhone", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common
          .getMatchingUsersPhone(data, originalId)
          .then(function (matchingItems) {
            if (matchingItems.totalFound > 0) {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                  },
                ])
              );
            } else {
              resolve(true);
            }
          });
      } else {
        console.log("Phone not provided");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateUniquenessBName", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common
          .getMatchingBName(data, originalId)
          .then(function (matchingItems) {
            if (matchingItems.totalFound > 0) {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                  },
                ])
              );
            } else {
              resolve(true);
            }
          });
      } else {
        console.log("buisness_name not provided");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateUniquenessUsername", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common
          .getMatchingUsersUsername(data, originalId)
          .then(function (matchingItems) {
            if (matchingItems.totalFound > 0) {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                  },
                ])
              );
            } else {
              resolve(true);
            }
          });
      } else {
        console.log("Username not provided");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateUniquenessUPC", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common
          .getMatchingUPC(data, originalId)
          .then(function (matchingItems) {
            if (matchingItems.totalFound > 0) {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                  },
                ])
              );
            } else {
              resolve(true);
            }
          });
      } else {
        console.log("upc not provided");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateUniquenessName", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common
          .getMatchingRolesName(data, originalId)
          .then(function (matchingItems) {
            if (matchingItems.totalFound > 0) {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyExists",
                  },
                ])
              );
            } else {
              resolve(true);
            }
          });
      } else {
        console.log("Name not provided");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateInResolution", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data && schema.client_id) {
        db.resolution.resolutionGetById(data, schema.client_id).then(function (result) {
          if (result.totalFound > 0) {
            if (result.items[0].client_id === schema.client_id) {
              resolve(true);
            } else {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "itemDoesNotMatchClient",
                  },
                ])
              );
            }
          } else {
            reject(
              new Ajv.ValidationError([
                {
                  keyword: "itemDoesNotExist",
                },
              ])
            );
          }
        });
      } else {
        console.log("Schema no provisto de tabla o propiedades.");
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateExistence", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data && schema.table) {
        db.common.getById(data, schema.table).then(function (result) {
          if (result.totalFound > 0) {
            if (schema.mall) {
              db.common
                .getById(result.items[0].mall_id, "mall")
                .then(function (result_mall) {
                  if (result.totalFound > 0) {
                    rootData.slot.mall_id = result_mall.items[0].id;
                    rootData.slot.entity = result_mall.items[0].entity;
                    resolve(true);
                  } else {
                    reject(
                      new Ajv.ValidationError([
                        {
                          keyword: "itemDoesNotExist",
                        },
                      ])
                    );
                  }
                });
            } else if(schema.client_id) {
              if(schema.client_id == result.items[0].client_id){
                resolve(true);
              }else{
                reject(
                  new Ajv.ValidationError([
                    {
                      keyword: "itemDoesNotExistForUser",
                    },
                  ])
                );
              }              
            } else {
              resolve(true);
            }
          } else {
            reject(
              new Ajv.ValidationError([
                {
                  keyword: "itemDoesNotExist",
                },
              ])
            );
          }
        });
      } else {
        console.log("Schema no provisto de tabla o propiedades.");
        resolve(true);
      }
    }),
});

ajv.addKeyword("editableSurvey", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
        db.common.getById(data, 'survey').then(function (result) {
          if (result.totalFound > 0) {
            if(result.items[0].url==null || result.items[0].url==""){
              resolve(true);
            } else {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "surveyNotEditable",
                  },
                ])
              );
            }
          } else {
            reject(
              new Ajv.ValidationError([
                {
                  keyword: "itemDoesNotExist",
                },
              ])
            );
          }
        });
    }),
});

ajv.addKeyword("validateDelete", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data && schema.table && rootData.type == "delete") {
        db.common.getById(data, schema.table).then(function (result) {
          if (result.items[0].is_active == false) {
            resolve(true);
          } else {
            reject(
              new Ajv.ValidationError([
                {
                  keyword: "itemStillActive",
                },
              ])
            );
          }
        });
      } else {
        resolve(true);
      }
    }),
});

ajv.addKeyword("validateStock", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.id) {
          var originalId = rootData.id;
        }
        db.common.getById(originalId, "slot").then(function (matchingItems) {
          console.log(matchingItems.items[0]);
          if (data > matchingItems.items[0].stock_limit) {
            reject(
              new Ajv.ValidationError([
                {
                  keyword: "overStockLimit",
                  dataPath: ".stock",
                  params: { format: "stock" },
                },
              ])
            );
          } else {
            resolve(true);
          }
        });
      } else {
        resolve(true);
      }
    }),
});

ajv.addKeyword("notToday", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        let activation_date = new Date(data);
        let today = new Date();
        if (activation_date > today) {
          resolve(true);
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "activationInThePast",
                dataPath: ".activation_date",
                params: { format: "activation_date" },
              },
            ])
          );
        }
      } else {
        resolve(true);
      }
    }),
});

ajv.addKeyword("validatePeriod", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data && schema.table) {
        db.period.get({ client_id: data.client_id }).then(function (result) {
          let activation_date = new Date(data.activation_date),
            expiration_date = addMonths(new Date(activation_date), data.licence_months);
          switch (result.length) {
            case 2:
              if ((activation_date > result[0].expiration_date) && (expiration_date > result[0].expiration_date)) {
                resolve(true);
              } else {
                reject(
                  new Ajv.ValidationError([
                    {
                      keyword: "conflictOfPeriod",
                    },
                  ])
                );
              }
            case 1:
              let today = new Date();
              if (today > result[0].activation_date) {
                if ((activation_date > result[0].expiration_date) && (expiration_date > result[0].expiration_date)) {
                  resolve(true);
                } else {
                  reject(
                    new Ajv.ValidationError([
                      {
                        keyword: "conflictOfPeriod",
                      },
                    ])
                  );
                }
              } else {
                resolve(true);
              }
            case 0:
              resolve(true);
            default:
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "OverLimitPeriod",
                  },
                ])
              );
          }
        });
      } else {
        resolve(true);
      }
    }),
});

ajv.addKeyword("fileUniqueNameByClient", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) => new Promise((resolve, reject) => {
    if (schema && data) {
      let filter = {
        client_id: schema,
        name: data,
        is_deleted:false
      }
      db.common.hardGet(filter, 'advertisement').then((result)=>{
        if(result.length){
          reject(
            new Ajv.ValidationError([
              {
                keyword: "alreadyExists",
                dataPath: ".name"
              },
            ])
          );
        } else {
          resolve(true);
        };
      }, (error)=>{
        resolve(false);
      });
    } else {
      resolve(false);
    }
  }),
});

ajv.addKeyword("validateSurveyeeExistenceAndCompletion", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) => new Promise((resolve, reject) => {
    if (data) {
      let filter = {
        id: data,
        is_deleted:false
      }
      db.common.hardGet(filter, 'surveyee').then((result)=>{
        if(result.length){
          if(result[0].answered){
            reject(
              new Ajv.ValidationError([
                {
                  keyword: "alreadyAnswered",
                  dataPath: ".surveyee_id"
                },
              ])
            );
          } else {
            resolve(true)
          };
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "itemDoesNotExist",
                dataPath: ".surveyee_id"
              },
            ])
          );
        };
      }, (error)=>{
        resolve(false);
      });
    } else {
      resolve(false);
    }
  }),
});

function addMonths(date, months) {
  let d = date.getDate();
  date.setMonth(date.getMonth() + +months);
  if (date.getDate() != d) {
    date.setDate(0);
  }
  return date;
}

ajv.addKeyword("setDurationOn", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.set_duration) {
          resolve(true);
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "setDurationOn",
                dataPath: ".duration",
                params: { format: "duration" },
              },
            ])
          );
        }
      } else {
        resolve(true);
      }
    }),
});

ajv.addKeyword("setEffect", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) =>
    new Promise((resolve, reject) => {
      if (data) {
        if (rootData.effect != "Ninguno") {
          resolve(true);
        } else {
          reject(
            new Ajv.ValidationError([
              {
                keyword: "setEffect"
              },
            ])
          );
        }
      } else {
        resolve(true);
      }
    }),
});

ajv.addKeyword('idsList', {
  async: true,
  validate: (schema, data, parentSchema, currentPath, parentObject, propertyInParentObject, rootData) => new Promise((resolve, reject) => {
    /*
      This keyword verifies that data is a string in the form of a list of positive integers (for example: "1,2,50" o "3,1,9")
      and that each integer is an existing id in the table specified in schema, then .
      schema must be an object with the "table" property like:
      { "table": "log" }
    */
    try {
      const integersListPattern = "^[1-9][0-9]*(,[1-9][0-9]*)*$";
      if (new RegExp(integersListPattern).test(data)) {
        const dataSplitedToIntegers = data.split(',').map(x => parseInt(x));
        const newSchemaForExistance = {
          "$async": true,
          "type": "array",
          "uniqueItems": true,
          "items": {
            "validateExistence": schema
          }
        }
        ajv.validate(newSchemaForExistance, dataSplitedToIntegers).then((result) => {
          parentObject[propertyInParentObject] = dataSplitedToIntegers;
          resolve(true);
        }, ajvError => {
          ajvError.errors[0].dataPath = currentPath + ajvError.errors[0].dataPath;
          reject(ajvError)
        }).catch(tryCatchError => reject(newTryCatchError(tryCatchError)));
      } else {
        reject(
          new Ajv.ValidationError(
            [
              {
                keyword: "pattern",
                dataPath: currentPath,
                params: { pattern: integersListPattern },
                message: `should match pattern "${integersListPattern}"`
              }
            ]
          )
        );
      }
    } catch (tryCatchError) {
      reject(newTryCatchError(tryCatchError));
    }
  })
});

ajv.addKeyword("priorityOn", {
  async: true,
  validate: (
    schema,
    data,
    parentSchema,
    currentPath,
    parentObject,
    propertyInParentObject,
    rootData
  ) => new Promise((resolve, reject) => {
    if (schema && data) {
      if(data){
        if (rootData.hasOwnProperty("screen_id")){
          let filter = {
            screen_id: rootData.screen_id,
            priority: data,
            is_deleted:false
          }
          db.common.hardGet(filter, 'events').then((result)=>{
            if(result.length){
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "alreadyPriority",
                    dataPath: ".priority",
                    params: { result }
                  },
                ])
              );
            } else {
              resolve(true);
            };
          }, (error)=>{
            resolve(false);
          });
        }else if (rootData.hasOwnProperty("id")){
          db.common.getById(rootData.id, 'events').then((result)=>{
            if(result.totalFound > 0){
              let filter = {
                screen_id: result.items[0].screen_id,
                priority: data,
                is_deleted:false
              }
              db.common.hardGet(filter, 'events').then((resultId)=>{
                if(resultId.length){
                  if(rootData.id == resultId[0].id){
                    resolve(true);
                  }else{
                    reject(
                      new Ajv.ValidationError([
                        {
                          keyword: "alreadyPriority",
                          dataPath: ".priority",
                          params: { result }
                        },
                      ])
                    );
                  }
                } else {
                  resolve(true);
                };
              }, (error)=>{
                resolve(false);
              });
            } else {
              reject(
                new Ajv.ValidationError([
                  {
                    keyword: "itemDoesNotExist",
                    dataPath: ".id"
                  },
                ])
              );
            };
          }, (error)=>{
            resolve(false);
          });
        }else{
          reject(
            new Ajv.ValidationError([
              {
                keyword: "itemDoesNotExist",
                dataPath: ".id"
              },
            ])
          );
        }
      }else{
        resolve(false);
      }
    } else {
      resolve(false);
    }
  }),
});

module.exports = ajv;
