const fs = require('fs');

exports.delete = (file) => {
  return new Promise((resolve, reject) => {
    fs.unlink(file, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      };
    });
  });
};