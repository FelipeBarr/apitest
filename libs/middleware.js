const jwt = require("jsonwebtoken"); // used to create, sign, and verify tokens
const config = require("../config");
const db = require("../database/queryFunctions");
var responseThrower = require("./responseThrower.js").responseThrower;

exports.storageInit = (req, res, next) => {
  req.storage = {};
  next();
};

// Middleware to create the first log record.
exports.createLog = (req, res, next) => {
  let input = req.body || req.query;
  input = JSON.parse(JSON.stringify(input));
  delete input.password;
  let method = req._parsedUrl.pathname;
  db.logs.create(input, method).then((log) => {
    req.logId = log.id;
    next();
  });
};

// Middleware for route validation
exports.routesValidator = (routes) => {
  console.log('routes :>> ', routes);
  return (req, res, next) => {
    // Verificar que la ruta existe
    let pathname = req._parsedUrl.pathname.split(/\d+/);
    pathname = pathname.reduce((a, b, index) => {
      return a + ":id" + (index - 1) + b;
    });
    if (routes.includes(pathname)) {
      next();
    } else {
      responseThrower(res, req.logId, 404, { keyword: "incorrectRoute" });
    }
  };
};

// Middleware for token validation
exports.tokenValidator = (req, res, next) => {
  // check header or url parameters or post parameters for token
  if (req.headers && req.headers["refer"]) {
    var t = req.headers["refer"].split("?token=%");
    t = t[1];
  }
  var token = req.query.token || req.headers["token"] || t;

  // decode token
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, config.secretKey, (err, decoded) => {
      if (err) {
        responseThrower(res, req.logId, 401, { error: "invalidToken" });
      } else {
        // if everything is good, save to request for use in other routes
        req.decoded = decoded;
        db.logs.updatecreated_by(req.logId, decoded.id);
        var newToken = jwt.sign(
          {
            id: decoded.id,
            permissions: decoded.permissions,
            start: decoded.start,
          },
          config.secretKey,
          {
            //var token = jwt.sign({}, config.secretKey, {
            expiresIn: 3600, // expires in 1 hours
          }
        );
        res.append("Access-Control-Expose-Headers", "newToken");
        res.append("newToken", newToken);
        next();
      }
    });
  } else {
    responseThrower(res, req.logId, 401, { error: "tokenNotProvided" });
  }
};

// Middleware for check permissions
exports.permissionsValidator = (req, res, next) => {
  if(req.decoded.type=='api'){
    next();
  } else {
    db.users
    .getRoleByUserId(req.decoded.id)
    .then((result) => {
      if (result) {
        if (result.is_active) {
          var permissions = "";
          if (!result.is_verified || req.decoded.password_reset) {
            permissions = { resetPassword: "U" };
          } else {
            permissions = result.permissions;
          };
          req.storage.permissions = permissions;
          if (!result.active_license) {
            responseThrower(res, req.logId, 401, {
              error: "inactiveLicense",
            });
          } else if (isAllowed(req, permissions) || permissions.all) {
            next();
          } else {
            responseThrower(res, req.logId, 401, {
              error: "invalidPermissions",
            });
          }
        } else {
          responseThrower(res, req.logId, 401, { error: "invalidPermissions" });
        }
      } else {
        responseThrower(res, req.logId, 401, { error: "invalidPermissions" });
      }
    })
    .catch((error) => {
      responseThrower(res, req.logId, 401, { error: "invalidPermissions" });
    });
  };
};

const methods = {
  POST: "C",
  GET: "R",
  PUT: "U",
  DELETE: "D",
};

function isAllowed(req, dbPermissions) {
  const url = req._parsedUrl.pathname.split("/"); // Array of url's parts
  url.splice(0, 2); // Array of url's parts (only from pathname to the end)
  url.splice(1, url.length);
  const permissions = dbPermissions; // Decoded permissions

  var auxPermissions = permissions; // Iteration over all permissions to find the url permission
  for (index = 0; index < url.length; index++) {
    if (auxPermissions[url[index]]) {
      auxPermissions = auxPermissions[url[index]];
    } else {
      return false;
    }
  }

  if (typeof auxPermissions != "object") {
    // It means url is shortest than the permission tree
    return auxPermissions.includes(methods[req.method]);
  } else {
    return false;
  }
}
