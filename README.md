# NOBOT dashboard api

This is an APIs in nodejs 10.15.0, this API 
* use mysql 5.7.28, and server use mysql  Ver 15.1 Distrib 10.1.44-MariaDB
* has md5 hashing of passwords
* has token based authentication
* Role Based Access Control with roles assigned to each user, and each role have several permissions of the API routes.
* the API is documented with apiDoc.

## Requirements

* First install mysql.
* Install all npm packages, run in console `npm install`.
* Run `npm run db` to initial database.
* Run `npm run format` to create a dummy config.

## Config

* Before run, you need to set a config.js file:
- Fill express section with your own port and end point.
- Fill mysql information for database, the recomendation its use host:"localhost" and your root user and password.


## Commands
* To restart the database, please run in console `npm run db`.
* To run the API, please write `npm start`.